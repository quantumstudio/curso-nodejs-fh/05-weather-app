require('dotenv').config()

const { readInput, inquirerMenu, pause, listPlaces } = require('./helpers/inquirer');
const Searches = require('./models/searches');

const main = async () => {
  const searches = new Searches();
  let option;

  do {
    option = await inquirerMenu();

    switch (option) {
      case 1: {
        // Show message
        const placesToSearch = await readInput('Place: ');

        // Search places
        const places = await searches.places(placesToSearch);

        // Select place
        const selectedId = await listPlaces(places);
        if (selectedId === '0') {
          continue;
        }

        const selectedPlace = places.find(place => place.id === selectedId);

        // Save into DB
        searches.addHistory(selectedPlace.name);

        // Weather
        const { description,
                min,
                max,
                temp } = await searches.weatherByPlace(selectedPlace.lat, selectedPlace.lng);

        // Show results
        console.clear();

        console.log('\nCity information\n'.green);
        console.log('City:', selectedPlace.name.green);
        console.log('Lat:', selectedPlace.lat);
        console.log('Lng:', selectedPlace.lng);
        console.log('Temperature:', `${temp}℃`);
        console.log('Min:', `${min}℃`);
        console.log('Max:', `${max}℃`);
        console.log(`How's the day?:`, description.green);

        break;
      }

      case 2: {
        searches.listSearchHistory();
        break;
      }

      default: {
        break;
      }
    }

    if (option !== 0) {
      await pause();
    }
  } while (option !== 0);
}

main();
