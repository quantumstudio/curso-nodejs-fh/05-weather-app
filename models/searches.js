const fs = require('fs');
const axios = require('axios');

class Searches {
  searchHistory = [];
  dbPath = './db/database.json';

  constructor() {
    // If exist, read db
    this.readDB();
  }

  get historyCapitalize() {
    return this.searchHistory.map((place) => history.split(' ').map(this.capitalize).join(' '));
  }

  getParamsMapbox() {
    return {
      'access_token': process.env.MAPBOX_KEY,
      'limit': 5,
      'language': 'es'
    };
  };

  getParamsWeather(lat, lon) {
    return {
      'appid': process.env.OPENWEATHER_KEY,
      lat,
      lon,
      'lang': 'es',
      'units': 'metric'
    }
  };

  async places(place = '') {
    try {
      const instance = axios.create({
        baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${place}.json`,
        params: this.getParamsMapbox()
      });

      const response = await instance.get();

      return response.data.features.map((place) => ({
        id: place.id,
        name: place.place_name,
        lng: place.center[0],
        lat: place.center[1]
      }));
    } catch (error) {
      return [];
    }
  }

  async weatherByPlace(lat, lon) {
    try {
      const instance = axios.create({
        baseURL: 'https://api.openweathermap.org/data/2.5/weather',
        params: this.getParamsWeather(lat, lon)
      });

      const { data } = await instance.get();
      const { main, weather } = data;

      return {
        description: weather[0].description,
        min: main.temp_min,
        max: main.temp_max,
        temp: main.temp
      };
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  addHistory(place = '') {
    // We check if the place already exists in the search history.
    if (!this.searchHistory.includes(place.toLowerCase())) {
      this.searchHistory = this.searchHistory.slice(0, 5);
      this.searchHistory.unshift(place.toLowerCase());
    }

    // Save into db
    this.saveDB();
  }

  listSearchHistory() {
    this.historyCapitalize.forEach((place, index) => {
      const i = `${index + 1}`.green;
      console.log(`${i}. ${place}`);
    });
  }

  saveDB() {
    const payload = {
      history: this.searchHistory
    };

    fs.writeFileSync(this.dbPath, JSON.stringify(payload));
  }

  readDB() {
    if (fs.existsSync(this.dbPath)) {
      const info = fs.readFileSync(this.dbPath, { encoding: 'utf-8' });
      const {history} = JSON.parse(info);
      this.searchHistory = [...history];
    }
  }

  capitalize(str = '') {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}

module.exports = Searches;
